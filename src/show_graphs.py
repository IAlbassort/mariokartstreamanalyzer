import sqlite3
import sys
from typing import List, Literal
import matplotlib.pyplot as plt
import numpy as np
from lib.compile import get_model_dict, unpack_rank_event
from lib.classes import RankEvent

def unpack_rank_event_without_rollback(events):
    x_output = []
    y_output = []
    for _, _, _, rank, frame_start, frame_end in events:
        if rank == 13: continue
        for x in range(frame_end - frame_start):
            x_output.append(rank)
            y_output.append(frame_start+x) 
    return (x_output, y_output)


def get_compile_version(cur, race_id, compile_version = -1):
    compile_event_for_race = cur.execute("select Id, ModelId from CompileEvent where RaceId = ?", (race_id,)).fetchall()
    version = ()
    if compile_version == -1:
        version = compile_event_for_race[len(compile_event_for_race)-1]
    else:
        version = compile_event_for_race[compile_version-1]
    return version

def show_simple_graph(race_id, cur, compile_version = -1):
    compile_event, model_id = get_compile_version(cur, race_id, compile_version)
    event_values_prepare = cur.execute("select Rank, StartFrame, EndFrame from RaceEvent where RaceId = ? and CompileEventId = ? ", (race_id,compile_event))
    event_values = event_values_prepare.fetchall()
    event_values = list(map(lambda x: RankEvent(x[1], x[0], x[2]), event_values))
    x = unpack_rank_event(event_values)

    plt.stairs(x, zorder=2)
    plt.ylim((1, 12))
    rank_ticks = np.arange(0, 14, 1)
    plt.xlabel("Frames")
    plt.ylabel("Rank")
    plt.yticks(rank_ticks)
    plt.show()
    
def show_simple_graph_with_interpolation_markers(race_id, cur, compile_version = -1):

    compile_event, model_id = get_compile_version(cur, race_id, compile_version)

    event_values_prepare = cur.execute("select Rank, StartFrame, EndFrame from RaceEvent where RaceId = ? and CompileEventId = ? ", (race_id,compile_event))
    event_values = event_values_prepare.fetchall()
    event_values = list(map(lambda x: RankEvent(x[1], x[0], x[2]), event_values))
    x= unpack_rank_event(event_values)
    events_start = list(map(lambda x: x.frame_start, (filter(lambda x: x.rank != 13, event_values))))
    bad_start = list(map(lambda x: x.frame_start, (filter(lambda x: x.rank == 13, event_values))))

    plt.vlines(bad_start, ymin=0, ymax=13, colors="red", linestyles=(0, (3, 12)))
    plt.vlines(events_start, ymin=0, ymax=13, colors="green", linestyles=(0, (3, 12)))
    plt.stairs(x, zorder=2)
    plt.ylim((1, 12))
    rank_ticks = np.arange(0, 14, 1)
    plt.xlabel("Frames")
    plt.ylabel("Rank")
    plt.yticks(rank_ticks)
    plt.show()

def show_debug_graph(race_id, cur, compile_version = -1):
    fig, axs = plt.subplots(2,  sharex='col', gridspec_kw={'height_ratios': [1, 3]})

    compile_event, model_id = get_compile_version(cur, race_id, compile_version)
    referenceCoords = get_model_dict(cur, model_id)[1]
    debug_values_prepare = cur.execute(
        """select One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twelve, Frame from ModelPerformance where RaceId = ? and CompileEventId = ? order by Frame ASC"""
        , (race_id,compile_event))
    debug_values = debug_values_prepare.fetchall()
    event_values_prepare = cur.execute("select * from RaceEvent where RaceId = ? and CompileEventId = ? ", (race_id,compile_event))
    event_values = event_values_prepare.fetchall()

    colors = {13 : "red", 1 : "gold", 2 : "silver", 3 : "brown", 4 : "blue", 5 : "green", 6 : "yellow", 7 : "darkviolet", 8 : "slategrey", 9 : "pink", 10 : "yellowgreen", 11 : "cyan", 12 : "black"}
    for y in range(0, 12):
        total = len(referenceCoords[y+1])
        values = (list(map(lambda x : x[y], debug_values)))
        percent = list(map(lambda x: (x / total)*100, values))
        axs[1].plot(percent, color=colors[y+1], label=f"{y+1}")

    x,y = unpack_rank_event_without_rollback(event_values)
    print
    axs[0].scatter(y, x)
    axs[0].set_ylabel("rank")
    axs[1].set_ylabel("Rank score as percentage")
    rank_ticks = np.arange(1, 13, 1)

    axs[0].set_yticks(rank_ticks)
    plt.legend(bbox_to_anchor=(1.02, 1), loc="upper left")
    plt.xlabel("Frame Of Video")
    plt.show()

def base(graph_type : Literal["simple", "debug"], race_id, compile_version = -1):
    con = sqlite3.connect("./db/mariokart.db")
    cur = con.cursor()

    match graph_type:
        case "debug":
            show_debug_graph(race_id, cur, compile_version)
        case "simple":
            show_simple_graph(race_id, cur, compile_version)
        case "interpolation":
            show_simple_graph_with_interpolation_markers(race_id, cur, compile_version)

graphs = ["debug", "simple", "interpolation"]
help = f"""
ARG1 = Literal{graphs} (Mandatory) NOTE: debug requires debug set to true in compile.py when compiled! 
ARG2 = INTEGER (> 0) Race Id (Mandatory)
ARG3 = INTEGER (> 0) Compile Version (Optional)
"""
try:
    arg1 = sys.argv[1]
except:
    quit(f'failed to get graph type.\n{help}')
if arg1 not in graphs:
    quit(f'graph must be of type {graphs}.\n{help}')
try:
    arg2 = sys.argv[2]
    arg2 = int(arg2)
    if 0 >= arg2:
        quit()

except:
    quit(f'failed to get arg2.\n{help}')

if len(sys.argv) == 4:
    try:
        arg3 = int(sys.argv[3])
        if 0 >= arg3:
            quit()
    except:
        quit(f'failed to convert arg3 to int.\n{help}')
else:
    arg3 = -1
base(arg1, arg2, arg3)