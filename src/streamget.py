from lib.pixget import pix
from lib.compile import compile
import cv2
import os
import sys
import time
import shutil as shutil
from multiprocessing import Process
from stringcolor import cs 
import yt_dlp
import datetime
import math
import copy
import numpy as np
from lib.classes import State, ParserState as ps
total = 0
date = str(datetime.datetime.now()).split(" ")[0]
load = 0
count = 0
framefail = 0
frames = 0
arg1 = sys.argv[1]
youtube = False
channel = None

help = "Argument 1: Should be a valid youtube URL. If you wish to capture a twitch stream use ./streamget.sh, arg1 should be https://twitch.tv/channelname"
#twitch shouldn't be passed into arg1
if "twitch.tv/" in arg1:
    print(help)
    exit()

elif "youtube.com/" in arg1:
    youtube = True
    ydl_opts = {
        'ignoreerrors': False,
        'abort_on_unavailable_fragments': True,
        'format': 'mp4[height=360]',
        'outtmpl': 'vodtemp/%(uploader)s/%(timestamp)s/%(id)s.mp4',
        "ratelimit": None,
        "fps" : 30
    }
    if ydl_opts is not None:
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            ydl.download(arg1)
else:
    arg2 = sys.argv[2]
    twichsplit = arg2.split("/")
    if twichsplit[1] == "popout":
        channel = twichsplit[2]
    else:   
        channel = twichsplit[1]
    if not channel:
        print(help)
        exit()




#a custom generator to make code better.
def return_captures(youtube : bool):
    if youtube:
        authors = list(os.walk("vodtemp/",))
        videos = []
        for x in authors:
            print(x[2])
            for y in x[2]:
                if ".mp4" in y:
                    videos.append(x[0] + "/" + y)
        for x in videos:
            _, channel, time, filename = x.split("/")
            yield (x, time, channel)
        return
    else:
        #TODO: RE-IMPLEMENT TWITCH
        return
    return

def process(img): 
    #saves time by manipulating less data.
    img = img[280:360, 530:640]
    #we crop, then convert the input to a black and white image, then convert it to a binary
    bw = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    processed = cv2.threshold(bw, 140, 255, cv2.THRESH_BINARY)[1]
    return processed

def getOutDir(channel, date):
    #this just looks better than f strings for paths.
    base_output = "archive/"+channel+"/"+date
    total = 0
    if os.path.isdir(base_output):
        total = len(os.listdir(base_output))
    else:
        os.makedirs(base_output)
    #ordered like this
    #archive=>channel=>date=>total amount of games
    #ie. archive/joel/2020-25-8/0
    return f"{base_output}/{total}/"

def skip_frames(capture, skip : int):
    for _ in range(0, skip):
        capture.read()


def iterate_state(state: State, frame):
    state.state_previous = state.state
    state.state = pix(frame, state.state)

#Generally, we want to avoid explicitly setting state because this script doesn't know anything about the image
#But we shouldn't add so much edge cases to pixget when we can simply change it manually.

def set_state(state: State, newState : ps) :
    state.state_previous = state.state
    state.state = newState
#def state_change_timeout(state: State, capture, window : int):
#    for _ in range(0, window):
#        captured, frame = cap.read()
#        iterate_state(state, frame)
#        if state.state_previous != state.state:
threads = []

for video, date, channel in return_captures(youtube):
    reading = True
    reset_switch = True
    state = State(channel, date, "./db/mariokart.db")
    frame_count = 0
    while reading:
        if reset_switch:
            print('capture!\n')
            cap = cv2.VideoCapture(video)
            reset_switch = False
            fps = cap.get(cv2.CAP_PROP_FPS)
            #sometimes its not quite 30
            frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)

            if math.ceil(fps) != 30:
                print(fps)
                print(video)
                print("ONLY 30FPS FOOTAGE IS ALLOWED")
                reading = False

        captured, frame = cap.read()
        if not captured:    
            print('capture ended!')
            os.remove(video)
            reading = False
            break
        iterate_state(state, frame)

        match state.state:
            case ps.Init_State:
                #if, we skip 45 frames to check if the game has even begun, skipping 3 frames when nothing is happening isn't a big deal
                skip_frames(cap, 3)

                continue

            case ps.Potential_Game_Begin:
                #sometimes there is a false positive, so if we skip 25 frames and it is still reading as 
                #the start of a game, its very likely to be so.

                skip_frames(cap, 45)
                captured, frame = cap.read()
                if not captured: break
                #CV2 is written well, in c++, so i assume that the frames are dealloc() after it leaves scope
                #It wont save the file unless i copy it into py's memory. 
                state.player_picture = copy.deepcopy(frame)


            case ps.Race_Start_Not_Detected_Yet:
                skip_frames(cap, 200)
                #Window for timeout
                for _ in range(0, 1300):
                    captured, frame = cap.read()
                    if not captured:
                        break
                    if pix(frame, state.state) == ps.Race_Start_Detected:
                        break

            case ps.Race_Start_Detected:
                print ('we enter!')
                state.current_frame = 0
                state.sub_race +=1
                state.video_data = []
                set_state(state, ps.Ongoing_Race)

            case ps.Ongoing_Race:
               #We deepcopy just encase it gets cleared.
                state.video_data.append(copy.deepcopy(process(frame)))
                print(f'{state.channel_name}-{state.date}-{state.current_frame}-{state.sub_race}', end='\r')
                #If the race doesn't end, just give up, I don't think theres a race this long
                if state.current_frame >= 5400:
                    set_state(state, ps.Init_State)
                state.current_frame += 1

            case ps.End_Of_Race:

                #this adds a compile and then starts it.
                #we add it encase the application exists before the last 1 or 2 finish.
                skip_frames(cap, 60)
                captured, frame = cap.read()
                if captured:
                    cv2.imwrite(f"{state.date}-{state.sub_race}.jpg", process(frame))    

                threads.append(Process(target=compile, args=(state,)))
                threads[-1].start()
                print('\nend_of_race\n')
                set_state(state, ps.Init_State)
                
                del state.video_data
                if not youtube: reset_switch = True        

#shutil.rmtree("vodtemp")
#NO COMPILE LEFT BEHIND!
for compile in threads:
    compile.join()

shutil.rmtree('vodtemp/')