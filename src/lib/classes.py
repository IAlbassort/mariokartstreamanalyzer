from enum import Enum
from typing import List
import cv2
import os
class ParserState(Enum):
    Init_State = 1
    Potential_Game_Begin = 2
    Race_Start_Not_Detected_Yet = 3
    Race_Start_Detected = 4
    Ongoing_Race = 5
    End_Of_Race = 6

class RankEvent:
    def __init__(self, frame_start, rank, frame_end = 0):
        self.frame_start = frame_start
        self.frame_end = frame_end
        self.rank = rank
    def __str__(self):
        return f"frame_start={self.frame_start}; frame_end={self.frame_end}; rank={self.rank}"
    def __repr__(self):
        return f"frame_start={self.frame_start}; frame_end={self.frame_end}; rank={self.rank}"
class ProcessReturn:
    def __init__(self, order, scores):
        self.order : int = order
        self.scores = scores
class State:
    def __init__(self, channel_name, date, db_path):
        self.current_frame = 0
        self.state = ParserState.Init_State  
        self.state_previous = ParserState.Init_State
        #If we're moving to databases, nothing should touch the disk
        #Therefore; an array of all the data makes more sense
        #TODO: Make somewhat to make this a np.array, of a fixed size, to make it a less costly 'append'....
        #np doesn't like cv2.Mat
        self.video_data : List[cv2.typing.MatLike] = []
        self.player_picture : cv2.typing.MatLike
        self.channel_name = channel_name
        self.date = date
        self.sub_race = -1
        self.db_path = os.path.abspath(db_path)