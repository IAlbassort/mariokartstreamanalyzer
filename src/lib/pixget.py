##  This script is the hard coded computer vision state detection mechanism for the program
##  All of the pixels chosen aren't decided scientifically, they are "magic numbers"
##  Numbers which, are consistent
##  They are all composite, of multiple of pixel values, their colors, checked within ranges
##  This is to account for the differences in brightness, saturation, and capture methods
##  This method is **only** reliable in a situation wherein, there are no graphs or other 
##  Things to obscure the screen.
##  Also, when the cropping and resolution is in the right position
##  The benefit, is that it is __fast__ __but__ is bottlenecked by opening the images 
##  A more flexible, but likely slower method would be edge detection of various pop-up graphics

from PIL import Image
from enum import Enum
from lib.classes import ParserState as ps
import pytesseract

#makes it more readable as you cant refer to enums without their owner object in python

def pix(frame, current_state : ps):
    img = Image.fromarray(frame)   
    match current_state:
        #Detects the players screen, which happens directly before a race starts
        case ps.Init_State | ps.Potential_Game_Begin:
            btwtul = [img.getpixel((310, 32)), img.getpixel((321, 293   )), img.getpixel((420, 330))]
            ib1 = 680 <= sum(btwtul[0]) <= 770
            ib2 = 0 <= sum(btwtul[1]) <= 55
            ib3 = 140 <= btwtul[2][0] <= 230
            ib4 = 140 <= btwtul[2][1] <= 230
            ib5 = 140 <= btwtul[2][2] <= 230
            ibl1 = (ib1, ib2, ib3, ib4, ib5)  
    
            if all(ibl1):
                #init state->potential-> race start
                match current_state:
                    case ps.Init_State:
                        return ps.Potential_Game_Begin
                    case ps.Potential_Game_Begin:
                        return ps.Race_Start_Not_Detected_Yet
            else:
                # kicks it back to init if it fails twice
                match current_state:
                    case ps.Init_State:
                        return ps.Init_State
                    
                    case ps.Potential_Game_Begin:
                        return ps.Init_State
        case ps.Race_Start_Not_Detected_Yet:
            #Detects the big "GO!" that happens when a race starts
            gox1 = img.getpixel((260,150)); gox2 = img.getpixel((330,145)); gox3 = img.getpixel((380,145))
            (b0, g0, r0) = gox1; (b1, g1, r1) = gox2; (b2, g2, r2) = gox3
            go1 = 200 <= r0 <= 255
            go2 = 200 <= r1 <= 255
            go3 = 200 <= r2 <= 255
            
            go4 = 180 <= g0 <= 225 
            go5 = 180 <= g1 <= 225 
            go6 = 180 <= g2 <= 225
            
            go7 = 0 <= b0 <= 70 
            go8 = 0 <= b1 <= 70 
            go9 = 0 <= b2 <= 70
            goftul = [go1, go2, go3, go4, go5, go6, go7, go8, go9]
            if all(goftul) == True:
            #outputs -3 for upper script to interpret and route to init the race
                return ps.Race_Start_Detected
            #outputs -2 to signal the race hasn't started yet. It can interpret this as, it never starting
            return ps.Race_Start_Not_Detected_Yet
    
        case ps.Ongoing_Race:
            #End Check, only triggers while race in progress
            c7 = img.getpixel((354,133)); c8 = img.getpixel((420,156)); c9 = img.getpixel((220,152))
            (b6, g6, r6) = c7; 
            (b7, g7, r7) = c8; 
            (b8, g8, r8) = c9
            ib7 = 200 <= r6 <= 255
            ib8 = 200 <= r7 <= 255
            ib9 = 200 <= r8 <= 255
            ib10 = 205 <= g6 <= 255
            ib11 = 205 <= g7 <= 255
            ib12 = 205 <= g8 <= 255
            ib13 = 10 <= b6 <= 70 or 180 <= b6 <= 255 
            ib14 = 10 <= b7 <= 70 
            ib15 = 10 <= b8 <= 70
            ibl3 = (ib7, ib8, ib9, ib10, ib11, ib12, ib13, ib14, ib15)
            if all(ibl3):
                return ps.End_Of_Race
            return ps.Ongoing_Race


def get_players(img):
    img = Image.fromarray(img)
    #gets consistent player pixel locations
    place_tuples = [img.getpixel((220, 40)), img.getpixel((300, 82)), img.getpixel((300, 127)), img.getpixel((300, 172)), img.getpixel((300, 218)), img.getpixel(
    (300, 270)), img.getpixel((540, 40)), img.getpixel((540, 87)), img.getpixel((540, 130)), img.getpixel((540, 174)), img.getpixel((540, 223)), img.getpixel((540, 260))]

    pixelSums = []
    for place in range(0, len(place_tuples)):
       pixelSums.append(sum(place_tuples[place]))
    place = []

    #this seems unreliable.
    for x in range(0, 12):
        pib = 500 <= pixelSums[x] <= 800
        place.append(pib)
    if all(place):
        return 12   
    else:
        p = 0
        for x in range(0, 11):
            if place[x] == False:
                break
            p += 1
        return p
def ocr_core(file):
    #uses pytesseract to open an image and output string
    text = pytesseract.image_to_string(Image.fromarray(file)).rstrip()
    text = text.replace('\n', '')
    text = text.lstrip(' ')
    return text
if __name__ == '__main__':
    exit("This script is not to be ran standalone, and only exists as a module for streamget.py.")