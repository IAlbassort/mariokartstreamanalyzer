import cv2
import numpy as np
import pickle
from PIL import Image
from lib.pixget import get_players, ocr_core
from multiprocessing import Queue, Process
import more_itertools as more_itertools
import itertools as itertools
from thefuzz import fuzz
import matplotlib.pyplot as plt
from matplotlib.pyplot import text
from typing import List
import lz4.frame
import sqlite3
import hashlib
from lib.classes import RankEvent, ProcessReturn, State
debug = True


def get_2d_value(n, width):
    row = (n // width)
    col = (n % width)
    return (col, row)

def bools_to_int(a):
    assert len(a) == 8
    return sum(2**i for i, bit in enumerate(reversed(a)) if bit)



def int_to_bools(a : int):
    # Example 8-bit integer
    output = np.empty(8, dtype=np.uint8)
    assert 0 > a or 256 >= a
    bit_string = format(a, '08b')
    for i, x in enumerate(bit_string):
        output[i] = int(x)
    return output



def deserialize_bitfield_frame(bit_array, height, width):
    #In some circumstances its required for output, for 1 to be 255 rather than its actual bit value. Such as video encoding.
    frame = np.unpackbits(np.frombuffer(bit_array, dtype=np.uint8))
    return frame.reshape((height, width))

def deserialize_bitfield_video(bit_array, height, width):
    frame_size = height*width
    bit_frame_size = frame_size // 8
    output_frame_count = len(bit_array) / bit_frame_size
    #Make sure that the data is complete, it must evenly be devisable into frames 
    assert (output_frame_count % 1) == 0.0
    #floor it
    output_frame_count = int(output_frame_count)
    frames = np.frombuffer(bit_array, dtype=np.uint8)
    return np.unpackbits(frames).reshape((output_frame_count, height, width))




def process_ranks(events : List[RankEvent], minimum_length : int):
    #Removes transients of a given amount
    smooth : List[RankEvent] = list(filter(lambda x: (x.frame_end - x.frame_start) > minimum_length, events))
    grouped = []
    for rank, array in (itertools.groupby(smooth, lambda x: x.rank)):
        grouped.append(list(array))
    output = []
    for i, grouping in enumerate(grouped): 
        base = grouping[0]

        if len(grouping) == 1:
            if len(output) > 0:
                base.frame_start = output[-1].frame_end
                output.append(base)
            else:
                output.append(base)
            continue
        elif i == len(grouped)-1:
            base.frame_end = grouping[-1].frame_end
            if len(output) > 0:
                base.frame_start = output[-1].frame_end
            output.append(base)
            continue
        else:
            base.frame_end = grouping[-1].frame_end
            if len(output) > 0:
                base.frame_start = output[-1].frame_end
            output.append(base)
    return output
        

def unpack_rank_event(events : List[RankEvent]):
    print(events)
    output = []
    for i, event in enumerate(events):
        start = event.frame_start
        end = event.frame_end
        length = end-start
        if start == 0:
            length+=1
        rank = event.rank
        #13 means unknown
        if event.rank == 13:
            #if its all unknown then who cares
            if i == 0:
                output += ([events[i+1].rank] * length)
            else:
                output += ([events[i-1].rank] * length)
        else:
            output += ([rank] * length)

    return output

def plotPickle(inputFolder, course):
    with open(f"{inputFolder}/rank.p", 'rb') as pickle_file:
        a = pickle.load(pickle_file)
    plt.xlabel("Time")
    plt.ylabel("Position")
    plt.plot(a)
    avg = (sum(a)/len(a))
    plt.axhline(avg)
    text(avg, avg, "Average Position")
    plt.title(f'{course} | {inputFolder}')
    plt.savefig(f"{inputFolder}/rank.png")

def similar(a,b):
    return (fuzz.ratio(a,b))

def process_player_picture(img):
    img = img[320:345, 160:460]
    #we crop, then convert the input to a black and white image, then convert more_itertools to a binary
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.threshold(img, 140, 255, cv2.THRESH_BINARY)[1]
    return img

def process_frames(frames, referenceCoords, width, height, Q):
    
    for image, count in frames:
        rank_scores = []
        for rank in referenceCoords:
            holder = []
            #iterates over the dictionary that the models are stored in
            for height, width in referenceCoords[rank]:
                #more_itertools is a binary image, but cv2 treats more_itertools as a typical 3 per pixel byte-video.
                #each byte is the same so 0 == 1 and 1 == 2.
                value = int(image[width, height])
                holder.append(value == 255)
            #counts the off, or the "incorrect" colors, as the models are generated from on only samples.
            #adds its rank and the count, ie [(1, 0), (2, 692), (3, 700)...]
            rank_scores.append((rank, holder.count(0)))
        rank_scores.sort(key=lambda x: x[1])
        Q.put_nowait(ProcessReturn(count, rank_scores))
    return 


def get_model_dict(cur, id = -1):
    if id == -1:
        default_model_prepare = cur.execute("select * from Model order by id desc limit 1")
        id = default_model_prepare.fetchone()[0]
    rank_data_prepare = cur.execute("select Rank, X, Y from ModelData where ModelId = ?", (id,))
    rank_data = rank_data_prepare.fetchall()
    output = {}
    for x, y in itertools.groupby(rank_data, key=lambda x: x[0]):
        output[x] = list(map(lambda x: x[1:3], y))
    del rank_data
    return (id, output)

def generate_compile_event(cur, con, race_id, model_id):
    cur.execute("INSERT INTO CompileEvent(RaceId, ModelId) VALUES(?, ?)", (race_id, model_id,))
    con.commit()
    return cur.lastrowid

def compile(state : State, model_id = -1, recompile = False, race_id = -1):
    con = sqlite3.connect(state.db_path)
    cur = con.cursor()
    model_id, referenceCoords = get_model_dict(cur, model_id)
    
    frame_count = len(state.video_data)
    height, width = Image.fromarray(state.video_data[0]).size
    
    #we split the frames into an array of 8 parts, to make threading actually function
    #TODO: Make threads configurable 
    identified_frames = list(map(lambda x: (x[1], x[0]), enumerate(state.video_data)))
    split_frames = list(more_itertools.chunked(identified_frames, 8))

    threads = []
    #Queues are python channels. They work fine.
    Q = Queue()
    for x in split_frames:
        threads.append(Process(target=process_frames, args=(x, referenceCoords, width, height, Q)))
    for x in threads:
        x.start()

    #waits for the exit of all little baby threads
    while frame_count != Q.qsize(): continue
    #gets q's output
    processing_output : List[ProcessReturn] = []

    for x in range(frame_count):
        processing_output.append(Q.get(timeout=10))

    #Video data is packed to bits then compressed for storage
    packed_video_data = lz4.frame.compress(np.packbits(np.array(state.video_data)).tobytes())
    #No longer needed, we need this out of memory ASAP
    del state.video_data
    processing_output.sort(key=lambda x: x.order) 

    #you might be wondering why im not just dumping the digits in an array like before
    #this allows me to identify the size of groups of digits without keeping two lists
    #you might ask why i dont just keep a un-roll backed list and just go without the padding 
    #to you, I say shush. It calculates better (if a tad less accurate) this way and looks better on graphs.
    #Frame data is organized [rank, score]
    output : List[RankEvent] = []
    def add_to_output(rank, frame):
        if len(output) != 0 and output[-1].rank == rank: 
            return
        else:
            if len(output) > 0:
                output[-1].frame_end = frame 
            output.append(RankEvent(frame, rank)) 

    for frame, frame_data in enumerate(processing_output):
        #if the image is all white or all black
            if frame_data.scores[0][1] == frame_data.scores[1][1]:
                add_to_output(13, frame)
        #if the image isn't greater than 40 wrong
            elif frame_data.scores[0][1] > 40:
                add_to_output(13, frame)
        #if the second most likely is within 20 of the first
            elif frame_data.scores[1][1] - frame_data.scores[0][1] < 20:
                add_to_output(13, frame)
        #1 is the most ambagious. If the answer is 1 and its not CERTAIN its 1, we skip
            elif frame_data.scores[0][0] == 1 and frame_data.scores[0][1] >= 20:
                add_to_output(13, frame)
            else:
                add_to_output(frame_data.scores[0][0], frame)
    output[-1].frame_end = len(processing_output)-1
    ranks = process_ranks(output, 1)

    if not recompile:
        courses = [
                    "Mario Kart Stadium", "Water Park", "Sweet Sweet Canyon", "Thwomp Ruins", "Mario Circuit", 
                    "Toad Harbor", "Twisted Mansion", "Shy Guy Falls", "Sunshine Airport", 
                    "Dolphin Shoals", "Mount Wario", "Cloudtop Cruise", "Bone Dry Dunes", 
                    "Bowser's Castle", "Rainbow Road", "Moo Moo Meadows", "GBA Mario Circuit",
                    "Cheap Cheap Beach", "Toad's Turnpike", "Dry Dry Desert", "Donut Plains 3", 
                    "Royal Raceway", "DK Jungle", "Wario Stadium", "Sherbert Land", 
                    "Music Park", "Yoshi Valley", "Tick-Tock Clock", "Piranha Plant Slide",
                    "Grumble Volcano", "N64 Rainbow Road", "Yoshi Circuit", "Excitebike", 
                    "Dragon Driftway", "Mute City", "Wario's Gold Mine", "SNES Rainbow Road", 
                    "Ice Ice Outpost", "Hyrule Castle", "Baby Park", "Cheese Land", 
                    "Wild Wood", "Animal Crossing", "Neo Bowser City", "Ribbon Road", 
                    "Super Bell Subway", "Big Blue", "Tour Paris Promenade ", "Tour Tokyo Blur", 
                    "Tour New York Minute", "Tour Sydney Sprint", "3DS Toad Circuiｓ", 
                    "DS Shroom Ridge ", "SNES Mario Circuit 3", "GBA Snow Land", "N64 Choco Mountain", 
                    "GBA Sky Garden", "N64 Kalimari Desert", "Wii Mushroom Gorge", "Wii Coconut Mall", 
                    "Tour Ninja Hideaway", "DS Waluigi Pinball", "Sky-High Sundae", "Piranha Plant Cove", 
                    "Yoshi's Island", "Tour Athens Dash", "Tour Rome Avanti", "Squeaky Clean Sprint", 
                    "Tour Madrid Drive", "Hyrule Circuit"
        ]
        ocr = ocr_core(process_player_picture(state.player_picture))
        #We make an index of which course is closest to the ocr output
        course_dice = []
        for course in courses:
            course_dice.append((course, similar(ocr,course)))
        #We sort, which default returns the lowest number
        course_dice.sort(key=lambda x: x[1])
        #so we grab the last index which is the highest number; the most similar.
        course = course_dice[-1][0]
        player_count = get_players(state.player_picture)
        player_data = state.player_picture.tobytes()
        
        sha256_hash = hashlib.sha256()
        sha256_hash.update(packed_video_data)
        video_archive_sha256 = sha256_hash.hexdigest()


    
        cur.execute("""INSERT INTO Race (FrameCount, CourseName, PlayerCount, UploadDate, Uploader, VideoDataBinaryLZ4, VideoSha256, PlayerImageJPG) 
                                VALUES(?, ?, ?, ?, ?, ?, ?, ?)""", 
                    (len(processing_output), course, player_count, state.date, state.channel_name, packed_video_data, video_archive_sha256, player_data))
        con.commit()
        race_id = cur.lastrowid
    
    compile_event_id = generate_compile_event(cur, con, race_id, model_id)
    for rank in ranks:
        cur.execute("INSERT INTO RaceEvent(RaceId, Rank, StartFrame, EndFrame, ModelId, CompileEventId) VALUES (?,?,?,?,?,?)", (race_id, rank.rank, rank.frame_start, rank.frame_end, model_id, compile_event_id))

    if debug:
        #processing_output was very much not made for this kind of re-arrangement 
        for x in processing_output: 
            #scores are now ordered one to twelve 
            x.scores.sort(key=lambda x: x[0])
            #selects only their scores 
            x.scores = [y[1] for y in x.scores]
        #combines them into a tuple with the frame and the raceId. 
        model_archive = list(map(lambda x: (race_id, model_id, compile_event_id, x[0],) + tuple(x[1].scores), enumerate(processing_output)))
        print(model_archive[0])
        cur.executemany("""
        INSERT INTO ModelPerformance(RaceId, ModelId, CompileEventId, Frame, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twelve) VALUES (?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?)""", 
        model_archive)
    con.commit()
    #delete big memory hogs, hoping to reduce memory usage.
    del processing_output



