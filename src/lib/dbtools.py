import sqlite3
import matplotlib.pyplot as plt
import pickle
import more_itertools as it
from lib.compile import deserialize_bitfield_frame, deserialize_bitfield_video, compile
import lz4.frame
import cv2
import numpy as np
from lib.classes import State
def get_archived_video(con, cur, race_id):
    video_prepare = cur.execute("select VideoDataBinaryLZ4 from Race where Id = ? ", (race_id,))
    video = video_prepare.fetchall()[0][0]
    decompression = lz4.frame.decompress(video)
    #videos are 110 * 80, or 1100 bits
    data = deserialize_bitfield_video(decompression, 80, 110)
    return data

#get_archived_video returns a video with bits like 0 to 1 but images are supposed to be 0 to 255
def prepare_video_for_export(video):
    oned = video.reshape(-1)
    oned[oned == 1] = 255
    return oned.reshape(video.shape)

def export_frames(data, start, end, path_out):
    for x in range(start, end):
        cv2.imwrite(f"{path_out}/out{x}.jpg", data[x])

def recompile(cur, con, race_id, model_id):
    info_prepare = cur.execute("select UploadDate, Uploader from Race where Id = ? ", (race_id,))
    date, channel_name = info_prepare.fetchone()
    video_data = prepare_video_for_export(get_archived_video(con, cur, race_id))
    state = State(channel_name, date, "./db/mariokart.db")
    state.video_data = video_data
    compile(state, model_id=model_id, recompile=True, race_id=race_id)

def print_image(a, inverted = False):
    axis = 0
    if inverted: axis = 1
    for row in np.rollaxis(a, axis, 0):
        pythonified = row.tobytes()
        output = map(lambda x: str(int(int(x) == 1)), pythonified)
        print(''.join(output))
#recompile(con, cur, 7, -1)
#for_export = prepare_video_for_export(get_archived_video(con, cur, race_id))
#data = np.concatenate(np.concatenate(for_export[583:583+20].reshape(4, 5, 80, 110), axis=1), axis=1)
#cv2.imwrite("temp/0.jpg", data)