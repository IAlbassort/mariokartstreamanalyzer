create table Race(
    Id INTEGER PRIMARY KEY ,
    FrameCount int NOT NULL,
    CourseName varchar NOT NULL,
    PlayerCount int NOT NULL,
    UploadDate DateTime NOT NULL,
    Uploader varchar NOT NULL,
    VideoDataBinaryLZ4 Blob NOT NULL,
    VideoSha256 Blob UNIQUE NOT NULL, 
    PlayerImageJPG Blob NOT NULL,
    DateArchived DateTime default CURRENT_TIMESTAMP
 );

Create Table Model(
    Id INTEGER PRIMARY KEY,
    TimeCreated DateTime default CURRENT_TIMESTAMP,
    ModelSha256 Blob UNIQUE NOT NULL

);

Create Table ModelData(
    ModelId INTEGER NOT NULL,
    Rank int NOT NULL,
    X int NOT NULL,
    Y int NOT NULL,
    FOREIGN KEY(ModelId) REFERENCES Model(Id)
);

Create Table CompileEvent(
    Id INTEGER PRIMARY KEY,
    ModelId INTEGER NOT NULL,
    RaceId INTEGER NOT NULL,
    TimeCreated DateTime default CURRENT_TIMESTAMP,
    FOREIGN KEY(RaceId) REFERENCES Race(Id),
    FOREIGN KEY(ModelId) REFERENCES Model(Id),
    CONSTRAINT DuplicationPrevention UNIQUE (RaceId, ModelId)
);

create table RaceEvent(
    RaceId INTEGER NOT NULL,
    ModelId INTEGER NOT NULL,
    CompileEventId INTEGER NOT NULL,
    Rank int NOT NULL,
    StartFrame int NOT NULL,
    EndFrame int NOT NULL,
    FOREIGN KEY(RaceId) REFERENCES Race(Id),
    FOREIGN KEY(ModelId) REFERENCES Model(Id)
    FOREIGN KEY(CompileEventId) REFERENCES CompileEvent(Id)
);

create table ModelPerformance(
    RaceId INTEGER NOT NULL,
    ModelId INTEGER NOT NULL,
    CompileEventId INTEGER NOT NULL,
    One Int NOT NULL,
    Two Int NOT NULL,
    Three Int NOT NULL,
    Four Int NOT NULL,
    Five Int NOT NULL,
    Six Int NOT NULL,
    Seven Int NOT NULL,
    Eight Int NOT NULL,
    Nine Int NOT NULL,
    Ten Int NOT NULL,
    Eleven Int NOT NULL,
    Twelve Int NOT NULL,
    Frame int NOT NULL,
    FOREIGN KEY(RaceId) REFERENCES Race(Id),
    FOREIGN KEY(ModelId) REFERENCES Model(Id),
    FOREIGN KEY(CompileEventId) REFERENCES CompileEvent(Id)
);


select distinct rank, (select count(*) from ModelData as md where md.rank = ModelData.rank and ModelId = 3) as count from ModelData where ModelId = 3