import os 
from PIL import Image
import base64
import sqlite3
import hashlib
import numpy as np
from lib.dbtools import print_image
#This is a much updated version of the older ID. This one marks the exit from the ascii days of this project
#As did before. you add your images into id/x (1 .. 2 .. 3), and include a bunch. One full black, and one full white at the bare minimum
#this generates a 0 and 1 representation of that data allowing for classifying. the images based upon this data.


#an index of all 1s so we can get an index of which colors to scan to get a guess of what the image is.
rank_to_pixelarray = {}
for digit in range(1, 13):
    one_holder = []
    images = []
    base = f"id/{digit}"
    files = os.listdir(base)
    im = Image.open(f'{base}/{files[0]}')
    for x in files:
        images.append(Image.open(f"{base}/{x}").load())
        
    #they should all be of uniform height so we use the first image as a reference.
    for y in range(0, im.size[1]):
        #for y in the height
        for x in range(0, im.size[0]):
            #for x in the width
            for image in images:
            #because of the binary image setup, it only returns 1 value.
            #encase it isn't binary
                value = image[x,y]
                if type(value) == tuple:
                    value = value[0]

                if value != 255:
                    break

                assert value != 255 or value != 0
                    
            #for-elses only execute if the for loop doesn't break 
            else:
                one_holder.append((x,y))

    empty = np.zeros((im.size[0], im.size[1]), dtype=np.uint8)

    for x in one_holder:
        empty[x] = 1
    print_image(empty, inverted=True)
    rank_to_pixelarray[digit] = one_holder

raw_string = str(rank_to_pixelarray)
encoded = raw_string.encode('ascii')
base64_encoded = base64.b64encode(encoded)
sha256_hash = hashlib.sha256()
sha256_hash.update(base64_encoded)
shasum = sha256_hash.hexdigest()
con = sqlite3.connect("./db/mariokart.db")
cur = con.cursor()
first_insert_prepare = cur.execute("INSERT INTO Model(ModelSha256) VALUES(?)", (shasum,))
con.commit()
id = cur.lastrowid
for rank in rank_to_pixelarray:
    #In any other circumstance, this would be a vulnerability.
    cur.executemany(f"INSERT INTO ModelData(ModelId, Rank, X,Y ) VALUES({id}, {rank}, ?, ?)", rank_to_pixelarray[rank])
con.commit()