#!/bin/bash


has_installed() {
    if [ "$(which "$1")" != "" ]; then
        return 0  
    else
        return 1 
    fi
}


os=$(cat /etc/os-release | grep '^ID=' | cut -d '=' -f 2)

case $os in
	"debian" | "ubuntu")
    sudo apt-get install tesseract-ocr ffmpeg sqlite3 -y
	;;
	"archlinux")
	sudo pacman -S tesseract ffmpeg sqlite3
	;;
	*)
	echo "Unknown OS. Please install sqlite3 tesseract-ocr ffmpeg into your PATH yourself". 
esac

./initdb.sh
echo "Please install require requirements.txt manually, to ensure it fits the circumstances of your python installation"
