# Mario-Kart-Stream-Analyzer
**By Caroline Marceano**

A python script that watches Mario Kart footage, and outputs rank data

# Presentation

I made a presentation on this project.

https://www.youtube.com/watch?v=1mWTM_Ow5Uc

It covers specifically the machine learning aspect, in detail.

If you're here from that, and you wanna checkout the code I was talking about

You can find it in /src/lib/compile.py


# Like this project and want to run it in your browser?

I've created a colab so you can run it ANYWHERE!

https://colab.research.google.com/drive/1LnJavpTZ4FaFDdSzeWXjakbTRBiH0vnD?usp=sharing


# Version 3.0 notes:
    1. Everything is now running in sqlite
    2. Extreme dataset improvement
    3. Completely restructured project
    4. Removed stats.py (you're gonna have to use SQL for now)
    5. Added recompilation, all videos are stored in very compressed archives (see /src/lib/dbtools, /src/db/schema/schema.sql)
    6. Debug graphs 
Currently twitch doesn't work.

# Getting Started
```bash
#Only works on Ubuntu, Debian and, arch, otherwise install the dependencies manually: pip, sqlite3, tesseract, ffmpeg
./installdeps.sh
pip install -r requirements.txt
cd src 
#extracts model data
tar -xf id.tar.gz
# generates models
python3 id.py
#generates data
#Enable debug manually in /src/lib/compile.py if you want a debug output!
python3 streamget.py https://www.youtube.com/watch?v=VoupVr5sBNU
python3 show_graphs.py simple 2 1
#if debug: python3 show_graphs.py debug 2 1

```

# Future?

I'm working on a presentation.